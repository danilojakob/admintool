package com.danilojakob.admintool.util;

import com.danilojakob.admintool.dto.UserDto;
import com.danilojakob.admintool.entity.User;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class ConverterTest {

    private Converter converter_;

    @Before
    public void setUp() {
        converter_ = new Converter();
    }

    @Test
    public void convertDtoToEntity() {
        UserDto dto = new UserDto("danjak", "danilo.murer@gmail.com", "test123");
        User entity = converter_.convertUserDtoToEntity(dto);
        assertNotNull(entity);
        assertEquals("danjak", entity.getUsername());
        assertEquals("danilo.murer@gmail.com", entity.getEmail());
        assertEquals("test123", entity.getPassword());
        assertEquals("test123", entity.getPassword());
    }

    @Test
    public void convertEntityToDto() {
        User entity = new User();
        entity.setEmail("danilo.murer@gmail.com");
        entity.setPassword("test123");
        entity.setUsername("danjak");
        UserDto dto = converter_.convertUserEntityToDto(entity);
        assertNotNull(dto);
        assertEquals("danjak", dto.getUsername());
        assertEquals("danilo.murer@gmail.com", dto.getEmail());
        assertEquals("test123", dto.getPassword());
        assertEquals("test123", dto.getPassword());
    }

    @After
    public void flush() {
        converter_ = null;
    }
}

package com.danilojakob.admintool.util;

import com.danilojakob.admintool.util.helper.HasherTest;
import com.danilojakob.admintool.util.helper.PropertyFileHelperTest;
import com.danilojakob.admintool.util.helper.ServerStatusIsActiveTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        PropertyFileHelperTest.class,
        ServerStatusIsActiveTest.class,
        HasherTest.class,
})
public class HelperTest {
    /* Suite class for all the helpers */
}

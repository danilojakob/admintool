package com.danilojakob.admintool.util;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        HelperTest.class,
        ConverterTest.class,
})
public class UtilTest {
    /* Suite class for the all tests in the util package */
}

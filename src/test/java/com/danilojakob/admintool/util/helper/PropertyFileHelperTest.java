package com.danilojakob.admintool.util.helper;

import com.danilojakob.admintool.AdminToolConstants;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import java.io.File;
import java.util.Properties;

public class PropertyFileHelperTest {

    private PropertyFileHelper propertyFileHelper_;

    @Before
    public void setUp() {
        propertyFileHelper_ = new PropertyFileHelper();
    }

    @Test
    public void savePropertiesTest() {
        //Set properties
        Properties properties = new Properties();
        properties.put("lastname", "jakob");
        properties.put("name", "danilo");
        properties.put("age", "17");
        //
        propertyFileHelper_.savePropertyFile(properties, "myProperties");
        File file = new File(AdminToolConstants.TEST_PROPERTY_FILE_PATH);
        assertTrue(file.exists());
    }

    @Test
    public void readPropertiesTest() {
        Properties properties = propertyFileHelper_.readPropertyFile("myProperties");
        assertNotNull(properties);
    }

    @After
    public void flush() {
        propertyFileHelper_ = null;
    }
}

package com.danilojakob.admintool.util.helper;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class HasherTest {

    private Hasher hasher_;

    @Before
    public void setUp() {
        hasher_ = new Hasher();
    }

    @Test
    public void hashTest() {
        String toHash = "Danilo";
        String hashed = hasher_.hash(toHash);
        assertEquals("2ac52d82f1c633c615ba018b8dcfc6458d7769a5e323a0d1e1d2041755520a34", hashed);
    }

    @After
    public void flush() {
        hasher_ = null;
    }
}

package com.danilojakob.admintool.util.helper;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ServerStatusIsActiveTest {

    private ServerStatusHelper serverStatusHelper_;

    @Before
    public void setUp() {
        serverStatusHelper_ = new ServerStatusHelper();
    }

    @Test
    public void isActiveTest() {
        boolean isActive = serverStatusHelper_.isServerActive("127.0.0.1");
        assertTrue(isActive);
    }

    @After
    public void flush() {
        serverStatusHelper_ = null;
    }
}

'use strict';
var adminToolApp = angular.module('admintool', ['navbar', 'admintool.services', 'admintool.controllers']);
adminToolApp.constant("CONSTANTS", {
    checkUser: "/user/check",
    addUser: "/user/add"
});
'use strict';

angular.module('admintool.services', []).factory('UserService', ["$http", "CONSTANTS", function($http, CONSTANTS) {
    var service = {};

    /**
     * Method for checking if user exists in the database
     * @param userDto User Entity
     * @returns {Promise<T | never>} HTTP-Request
     */
    service.checkUser = function (userDto) {
        return $http.post(CONSTANTS.checkUser, userDto).then(function (value) {
                window.location.href = "/";
        }).catch(function (reason) { window.location.href = "/register" });
    };

    /**
     * Method for adding user to the database
     * @param userDto User Entity
     * @returns {Promise<T | never>} HTTP-Request
     */
    service.addUser = function (userDto) {
        return $http.post(CONSTANTS.addUser, userDto).then(function (value) {
            window.location.href = "/login";
        }).catch(function (reason) { window.location.href = "/" })
    };
    return service;
}]);
'use strict';

var module = angular.module('admintool.controllers', []);

module.controller("UserController", ["$scope", "UserService",
    function ($scope, UserService) {
        $scope.userDto = {
            username: "",
            email: null,
            password: null
        };

        /**
         * Check if User exists in the database
         */
        $scope.checkUser = function () {
            UserService.checkUser($scope.userDto).then(function () {
                console.log("User Checked");
            }).catch(function (reason) { console.log(reason);});
        };

        /**
         * Add user into the database
         */
        $scope.addUser = function () {
            if($scope.userDto.username === "" || $scope.userDto.email == null || $scope.userDto.password == null) {
                document.getElementById("warning-register").style.display = "block";
                return;
            }
            if (document.getElementById("password1").value !== document.getElementById("password2").value) {
                document.getElementById("error-register").style.display = "block";
                return;
            }
            UserService.addUser($scope.userDto).then(function (value) {
                console.log("User added")
            }).catch(function (reason) { console.log(reason); })
        };
    }]);
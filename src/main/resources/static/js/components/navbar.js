var app = angular.module('navbar', []);
    app.directive("navbar", function () {
        return {
            restrict: 'E',
            transclude: true,
            template: "<!-- Navigation Bar -->\n" +
                "<div ng-transclude>\n" +
                "<nav class=\"navbar navbar-expand-md navbar-dark bg-dark\">\n" +
                "    <a href=\"/\" class=\"navbar-brand\">Home</a>\n" +
                "    <button type=\"button\" class=\"navbar-toggler\" data-toggle=\"collapse\" data-target=\"#navbarCollapse\">\n" +
                "        <span class=\"navbar-toggler-icon\"></span>\n" +
                "    </button>\n" +
                "    <div class=\"collapse navbar-collapse\" id=\"navbarCollapse\">\n" +
                "        <ul class=\"navbar-nav\">\n" +
                "            <li class=\"nav-item\"><a href=\"#\" class=\"nav-link\"><img src=\"icons/user.png\" alt=\"User\">  Profile</a></li>\n" +
                "            <li class=\"nav-item\"><a href=\"#\" class=\"nav-link\"><img src=\"icons/ping-pong.png\" alt=\"Ping-Pong\"/>  Status</a></li>\n" +
                "            <li class=\"nav-item\"><a href=\"#\" class=\"nav-link\"><img src=\"icons/database.png\" alt=\"Server\"/>  Server</a></li>\n" +
                "        </ul>\n" +
                "        <ul class=\"nav navbar-nav ml-auto\">\n" +
                "            <li class=\"nav-item\"><button type=\"button\" class=\"btn btn-outline-success\"><a href=\"login\" class=\"nav-link active\">Log-In</a></button></li>\n" +
                "        </ul>\n" +
                "    </div>\n" +
                "</nav>\n" +
                "</div>"
        };
    });
package com.danilojakob.admintool.util.helper;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Class for server status
 */
public class ServerStatusHelper {

    /**
     * Method for checking if a server is active
     * @param address {@link String} address that needs to be checked
     * @return {@link Boolean} if the server is active
     */
    public boolean isServerActive(String address) {
        try {
            InetAddress serverAddress = InetAddress.getByName(address);
            return serverAddress.isReachable(5000);
        } catch (UnknownHostException uHostEx) {
            System.err.println("Unknown host: " + uHostEx);
        } catch (IOException iOex) {
            System.err.println("IOException: " + iOex);
        }
        return false;
    }
}

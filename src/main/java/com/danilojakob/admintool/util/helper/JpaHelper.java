package com.danilojakob.admintool.util.helper;

import com.danilojakob.admintool.AdminToolConstants;
import com.danilojakob.admintool.entity.User;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import java.util.List;

public class JpaHelper implements AdminToolConstants {

    EntityManagerFactory emFactory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT);
    EntityManager  entityManager = emFactory.createEntityManager();

    /**
     * Check if a user exists in the database
     * @param email {@link String} E-Mail Address of the user
     * @param password {@link String} Hashed password of the user
     * @return {@link Boolean} If the user exists
     */
    public boolean userExists(String email, String password) {
        TypedQuery<User> query = entityManager.createQuery("SELECT a FROM User a WHERE a.email = :email AND a.password = :password", User.class);
        query.setParameter("email", email);
        query.setParameter("password", password);
        List<User> list = query.getResultList();
        return !list.isEmpty();
    }

    /**
     * Add user to the database
     * @param email {@link String} E-Mail of the user
     * @param username {@link String} Username of the user
     * @param password {@link String} Hashed password
     * @return {@link Boolean} If the user exists
     */
    public boolean addUser(String email, String username, String password) {
        User user = new User();
        user.setPassword(password);
        user.setEmail(email);
        user.setUsername(username);

        entityManager.getTransaction().begin();
        entityManager.persist(user);
        entityManager.getTransaction().commit();

        return true;
    }

}

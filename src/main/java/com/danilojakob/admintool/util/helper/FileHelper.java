package com.danilojakob.admintool.util.helper;

import com.danilojakob.admintool.AdminToolConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FileHelper implements AdminToolConstants {

    private File file_;
    private FileReader fileReader_;
    private FileWriter fileWriter_;

    private final Logger LOGGER_ = LoggerFactory.getLogger(FileHelper.class);

    public FileHelper() {
        file_ = new File(SESSION_FILE);
        try {
            fileWriter_ = new FileWriter(file_);
            fileReader_ = new FileReader(file_);
        } catch (IOException ex) {
            LOGGER_.error("Error while instancing the file writer, file reader and the session file");
        }
    }

    public boolean checkSession(String sessionId) {
        return false;
    }
}

package com.danilojakob.admintool.util;

import com.danilojakob.admintool.dto.UserDto;
import com.danilojakob.admintool.entity.User;

/**
 * Class for converting DTO to entity and entity to DTO
 */
public class Converter {

    /**
     * Convert {@link UserDto} to {@link User}
     * @param userDto {@link UserDto} to convert
     * @return {@link User} entity
     */
    public User convertUserDtoToEntity(UserDto userDto) {
        User user = new User();
        user.setUsername(userDto.getUsername());
        user.setEmail(userDto.getEmail());
        user.setPassword(userDto.getPassword());
        return user;
    }

    /**
     * Convert {@link User} to {@link UserDto}
     * @param user {@link User} to convert
     * @return {@link UserDto} dto
     */
    public UserDto convertUserEntityToDto(User user) {
        return new UserDto(user.getUsername(), user.getEmail(), user.getPassword());
    }
}

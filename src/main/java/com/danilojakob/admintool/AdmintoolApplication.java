package com.danilojakob.admintool;

import com.danilojakob.admintool.controller.UserController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class AdmintoolApplication {

    public static void main(String[] args) {
        SpringApplication.run(AdmintoolApplication.class, args);
    }

}

package com.danilojakob.admintool.service;

import com.danilojakob.admintool.dto.UserDto;

public interface UserService {

    /**
     * Method for checking if user exists inside the database
     * @param userDto {@link UserDto} user Client-Side Entity
     * @return {@link Boolean} if user exists
     */
    boolean checkUser(UserDto userDto);

    /**
     * Method for adding user to the database
     * @param userDto {@link UserDto} user Client-Side Entity
     * @return {@link Boolean} if hasError
     */
    boolean addUser(UserDto userDto);
}

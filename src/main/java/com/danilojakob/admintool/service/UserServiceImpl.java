package com.danilojakob.admintool.service;

import com.danilojakob.admintool.dto.UserDto;
import com.danilojakob.admintool.util.helper.JpaHelper;
import com.danilojakob.admintool.util.helper.Hasher;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    private JpaHelper jpaHelper_;
    private Hasher hasher_;

    private final Logger LOGGER_ = LoggerFactory.getLogger(UserServiceImpl.class);

    @Override
    public boolean checkUser(UserDto userDto) {
        jpaHelper_ = new JpaHelper();
        hasher_ = new Hasher();
        LOGGER_.info("Entered checkUser Method");
        return jpaHelper_.userExists(userDto.getEmail(), hasher_.hash(userDto.getPassword()));
    }

    @Override
    public boolean addUser(UserDto userDto) {
        jpaHelper_ = new JpaHelper();
        hasher_ = new Hasher();
        return jpaHelper_.addUser(userDto.getEmail(), userDto.getUsername(), hasher_.hash(userDto.getPassword()));
    }
}
package com.danilojakob.admintool;

public interface AdminToolConstants {

    //View Mapping
    String MAPPING_HOME = "/";
    String MAPPING_LOGIN = "/login";
    String MAPPING_REGISTER = "/register";
    String MAPPING_SERVER = "/server";
    String MAPPING_STATUS = "/server/status";
    String MAPPING_PROFILE = "/profile";
    String MAPPING_ERROR = "/error";

    //Request Mapping
    String MAPPING_USER = "/user";
    String MAPPING_CHECK_USER = "/user/check";
    String MAPPING_ADD_USER = "/user/add";

    //JPA
    String PERSISTENCE_UNIT = "myPU";

    //File paths
    String SESSION_FILE = System.getProperty("user.home") + "\\seissions.dat";

    //Test constants
    String TEST_PROPERTY_FILE_PATH = "C:\\Users\\JADA\\conf\\myProperties.properties";
    String TEST_PROPERTY_FILE_PATH_HOMR = "C:\\Users\\JADA\\conf\\myProperties.properties";
    String work = "C:\\Users\\Danilo Murer\\conf\\myProperties.properties";
}

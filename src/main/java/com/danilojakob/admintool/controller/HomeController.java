package com.danilojakob.admintool.controller;

import com.danilojakob.admintool.AdminToolConstants;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController implements AdminToolConstants {
    @RequestMapping(MAPPING_HOME)
    public String home() {
        return "index";
    }
}

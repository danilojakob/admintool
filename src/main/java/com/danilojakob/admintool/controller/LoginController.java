package com.danilojakob.admintool.controller;

import com.danilojakob.admintool.AdminToolConstants;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class LoginController implements AdminToolConstants {

    @RequestMapping(MAPPING_LOGIN)
    public String login() {
        return "login";
    }
}

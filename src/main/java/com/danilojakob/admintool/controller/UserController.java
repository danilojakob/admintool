package com.danilojakob.admintool.controller;


import com.danilojakob.admintool.AdminToolConstants;
import com.danilojakob.admintool.dto.UserDto;
import com.danilojakob.admintool.service.UserService;
import com.danilojakob.admintool.util.helper.Hasher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

@RestController
public class UserController implements AdminToolConstants {
    @Autowired
    UserService userService;

    private Hasher hasher_ = new Hasher();

    /**
     *  Method for getting the login Mapping
     * @param userDto {@link UserDto} user that needs to be checked
     * @return {@link ResponseEntity} with STATUS_CODE
     */
    @PostMapping(MAPPING_CHECK_USER)
    public void checkUser(@RequestBody UserDto userDto, HttpServletResponse response) {
        if (userService.checkUser(userDto)) {
            response.setStatus(200);
            Cookie cookie = new Cookie("user_auth", hasher_.hash(userDto.getEmail()));
            response.addCookie(cookie);
        } else {
            response.setStatus(400);
        }
    }

    /**
     * Method for adding User into the database
     * @param userDto {@link UserDto} user that needs to be added
     * @return {@link ResponseEntity} with STATUS_CODE
     */
    @PostMapping(MAPPING_ADD_USER)
    public void addUser(@RequestBody UserDto userDto, HttpServletResponse response) {
        if (userService.addUser(userDto)) {
            response.setStatus(200);
        } else {
            response.setStatus(400);
        }
    }
}

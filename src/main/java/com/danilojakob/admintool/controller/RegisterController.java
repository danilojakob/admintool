package com.danilojakob.admintool.controller;

import com.danilojakob.admintool.AdminToolConstants;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class RegisterController implements AdminToolConstants {
    @RequestMapping(MAPPING_REGISTER)
    public String register() {
        return "register";
    }
}
